﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp7.DB.Estoque;

namespace WindowsFormsApp7.Telas.Estoque
{
    public partial class CadastrarEstoque : Form
    {
        BindingList<EstoqueDTO> produtosCarrinho = new BindingList<EstoqueDTO>();


        public CadastrarEstoque()
        {
            InitializeComponent();
            CarregarCombos();
            
        }


        void CarregarCombos()
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueDTO> lista = business.Listar();

            comboBox2.ValueMember = nameof(EstoqueDTO.id);
            comboBox2.DisplayMember = nameof(EstoqueDTO.id_produto);
            comboBox2.DataSource = lista;
        }

      
        private void btnEmitir_Click(object sender, EventArgs e)
        {
            EstoqueDTO dto = new EstoqueDTO();
            
            dto.quantidade = textBox2.Text;
            

            EstoqueBusiness business = new EstoqueBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }

    

            

        private void button1_Click(object sender, EventArgs e)
        {
            Form menu = new Form();
            menu.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp7.DB.Pedido;
using WindowsFormsApp7.DB.Produto;

namespace WindowsFormsApp7.Telas
{
    public partial class FazerPedido : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();


        public FazerPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }


        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            comboBox2.ValueMember = nameof(ProdutoDTO.id);
            comboBox2.DisplayMember = nameof(ProdutoDTO.nome);
            comboBox2.DisplayMember = nameof(ProdutoDTO.preco);
            comboBox2.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = produtosCarrinho;
        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = comboBox1.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(textBox5.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.cliente = comboBox1.Text;
            dto.endereco = textBox3.Text;
            dto.telefone = textBox4.Text;
            dto.nascimento = DateTime.Now;

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }
    }
}

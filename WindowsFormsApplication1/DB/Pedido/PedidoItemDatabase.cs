﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Base;

namespace WindowsFormsApp7.DB.Pedido
{
    class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produto, id_pedido) VALUES (@id_produto, @id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.idproduto));
            parms.Add(new MySqlParameter("id_pedido", dto.idpedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Base;

namespace WindowsFormsApp7.DB.Pedido
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (nm_cliente, dt_nascimento, ds_telefone, ds_endereco) VALUES (@nm_cliente, @dt_nascimento, @ds_endereco, @ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.cliente));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();
            while (reader.Read())
            {
                PedidoConsultarView dto = new PedidoConsultarView();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.QtdItens = reader.GetInt32("qtd_itens");
                dto.Data = reader.GetDateTime("dt_venda");
                dto.Total = reader.GetDecimal("vl_total");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp7.DB.Pedido
{
   public class PedidoItemDTO
    {
        public int id { get; set; }

        public int idproduto { get; set; }

        public int idpedido { get; set; }

    }
}

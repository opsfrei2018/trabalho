﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Produto;

namespace WindowsFormsApp7.DB.Funcionarios
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }

        public List<FuncionarioDTO> Consultar(string produto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Consultar(produto);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }
    }
}

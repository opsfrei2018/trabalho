﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Base;

namespace WindowsFormsApp7.DB.Funcionarios
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_funcionario, dt_nascimento, ds_rg, cg_cargo, ds_telefone, ds_endereco) VALUES (@nm_funcionario, @ds_rg, @cg_cargo,@ds_endereco, @ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.nome));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("ds_rg", dto.rg));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_cargo", dto.cargo));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        internal List<FuncionarioDTO> Listar()
        {
            throw new NotImplementedException();
        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario 
                                 SET nm_funcionario  = @nm_funcionario,
                                     dt_nascimento   = @dt_nacimento,
                                     ds_rg           =@ds_rg,
                                     cg_cargo        =@cg_cargo
                                     ds_endereco     =@ds_endereco
                                     ds_telefone     =@ds_telefone
                                 WHERE id_funcionario = @id_funcioanrio";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.nome));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("ds_rg", dto.rg));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_cargo", dto.cargo));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FuncionarioDTO> Consultar(string funcionario)
        {
            string script = @"SELECT * FROM tb_funcioanrio WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", funcionario + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.id = reader.GetInt32("id_funcionario");
                dto.nome = reader.GetString("nm_funcionario");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.endereco = reader.GetString("ds_endereco");
                dto.rg = reader.GetString("ds_rg");
                dto.cargo = reader.GetString("cg_cargo");
                dto.telefone = reader.GetString("ds_telefone");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

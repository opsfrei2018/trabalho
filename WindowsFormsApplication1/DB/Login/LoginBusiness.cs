﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Base;

namespace WindowsFormsApp7.DB.Login
{
    class LoginBusiness
    {
        public bool Logar(string login, string senha)
        {
            LoginDatabase db = new LoginDatabase();
            return db.Logar(login, senha);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp7.DB.Produto;

namespace WindowsFormsApp7.Telas.Produto
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = new ProdutoDTO();
            dto.nome = textBox1.Text;
            dto.preco = Convert.ToDecimal(textBox2.Text);
            dto.tipo = textBox3.Text;

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Produto salvo com sucesso.", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp7.DB.Estoque;
using WindowsFormsApp7.DB.Produto;

namespace WindowsFormsApp7.Telas.Estoque
{
    public partial class CadastrarEstoque : Form
    {
        public CadastrarEstoque()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            ProdutoBusiness bus = new ProdutoBusiness();
            List<ProdutoDTO> lista = bus.Listar();

            comboBox2.ValueMember = nameof(ProdutoDTO.id);
            comboBox2.DisplayMember = nameof(ProdutoDTO.nome);
            comboBox2.DataSource = lista;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoDTO nome = comboBox2.SelectedItem as ProdutoDTO;

            EstoqueDTO dto = new EstoqueDTO();
            dto.quantidade = textBox2.Text;

            dto.produtoID = nome.id;

            EstoqueBusiness business = new EstoqueBusiness();
            business.Salvar(dto);

            MessageBox.Show("Produto salvo com sucesso.", "Adega", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }
    }
}

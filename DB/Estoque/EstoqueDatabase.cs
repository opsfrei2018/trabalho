﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Base;

namespace WindowsFormsApp7.DB.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_estoque (qtd_quantidade, id_produto) VALUES (@qtd_quantidade, @id_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("id_produto", dto.produtoID));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque 
                                 SET qtd_quantidade  = @td_quantidade,
                                     id_produto    = @id_produto,
                                   
                               WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", dto.id));
            parms.Add(new MySqlParameter("qtd_quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("id_produto", dto.produtoID));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_estoque WHERE id_porduto like @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.id = reader.GetInt32("id_estoque");
                dto.quantidade = reader.GetString("qtd_quantidade");
                dto.produtoID = reader.GetInt32("id_produto");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<EstoqueDTO> Listar()
        {
            string script = @"SELECT * FROM tb_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.id = reader.GetInt32("id_estoque");
                dto.quantidade = reader.GetString("qtd_quantidade");
                dto.produtoID = reader.GetInt32("id_produto");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

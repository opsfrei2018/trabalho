﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp7.DB.Estoque
{
   public class EstoqueDTO
    {
        public int id { get; set; }
        public int produtoID { get; set; }
        public string quantidade { get; set; }
    }
}

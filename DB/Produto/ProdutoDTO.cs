﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp7.DB.Produto
{
    class ProdutoDTO
    {
        public int id { get; set; }

        public string tipo { get; set; }

        public decimal preco { get; set; }

        public string nome { get; set; }
    }
}

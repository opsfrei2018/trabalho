﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp7.DB.Pedido
{
    class PedidoConsultarView
    {
        public int Id { get; set; }
        public string Cliente { get; set; }
        public string endereco { get; set; }
        public DateTime nascimento { get; set; }
        public string telefone { get; set; }
        public decimal Total { get; set; }

    }
}

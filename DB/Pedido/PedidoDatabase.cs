﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp7.DB.Base;

namespace WindowsFormsApp7.DB.Pedido
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (nm_cliente, ds_endereco, dt_nascimento, ds_telefone) VALUES (@nm_cliente, @ds_endereco, @dt_nascimento,@ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.cliente));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("dt_nascimento", dto.nascimento));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();
            while (reader.Read())
            {
                PedidoConsultarView dto = new PedidoConsultarView();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.endereco = reader.GetString("ds_endereco");
                dto.telefone = reader.GetString("ds_telefone");
                dto.nascimento = reader.GetDateTime("dt_nascimento");
                dto.Total = reader.GetDecimal("vl_total");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp7.DB.Pedido
{
    class PedidoDTO
    {
        public int id { get; set; }

        public string cliente { get; set; }

        public DateTime nascimento { get; set; }

        public string endereco { get; set; }

        public string telefone { get; set; }

    }
}
